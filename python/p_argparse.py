import argparse
import os
import subprocess
import sys

import p


def activate_project(project_name: str) -> None:
  command_queue = []

  project_active='P_PROJECT_{}'.format(project_name)
  if project_active in os.environ and os.environ[project_active] == 'active':
    command_queue.append('echo "Project \'{}\' already active"'.format(project_name))
  else:
    # If the shell has no built-in support for environment prompts (e.g. bash), use an own version.
    if 'P_USE_ZSH_PROMPT' not in os.environ or os.environ['P_USE_ZSH_PROMPT'] == '':
      if 'P_ORIGINAL_PS1' not in os.environ:
        command_queue.append('export P_ORIGINAL_PS1="$PS1"')
        command_queue.append('export PS1="(\$P_CURRENT_PROJECT) $PS1"')

    # Source the project activation script.
    command_queue.append('source "{}"'.format(os.path.join(p.config_path, project_name + '.sh')))
    command_queue.append('{}_activate'.format(project_name))

    # Export the project name.
    command_queue.append('export P_CURRENT_PROJECT="{}"'.format(project_name))
    command_queue.append('cpr')

    # Set flag that project is activated.
    command_queue.append('export {}="active"'.format(project_active))


  print(' && '.join(command_queue))


def deactivate_project(project_name: str) -> None:
  command_queue = []

  envs = list(filter(lambda x: x.startswith('{}_'.format(project_name)), os.environ.keys()))

  project_active='P_PROJECT_{}'.format(project_name)
  if project_active not in os.environ:
    command_queue.append('echo "Project \'{}\' is not active"'.format(project_name))
  else:
    # Source the project deactivation script.
    command_queue.append('source "{}"'.format(os.path.join(p.config_path, project_name + '.sh')))
    command_queue.append('{}_deactivate'.format(project_name))

    # Restore PS1 if it was overridden (e.g. for bash)
    if 'P_ORIGINAL_PS1' in os.environ:
      command_queue.append('export PS1="$P_ORIGINAL_PS1"')

    # Clean up environment variables.
    command_queue.append('unset P_ORIGINAL_PS1')
    command_queue.append('unset P_CURRENT_PROJECT')
    command_queue.append('unset {}'.format(project_active))

    # Clean up common environment variables for projcets.
    for e in envs:
        command_queue.append('unset {}'.format(e))

    # Clean up setup and build functions.
    command_queue.append('unset -f {}_setup 2> /dev/null || true'.format(project_name))
    command_queue.append('unset -f {}_build 2> /dev/null || true'.format(project_name))

  print(' && '.join(command_queue))


def new_project(project_name: str, project_path: str) -> None:
  if project_name in p.find_projects():
    print('echo "Project with the name \'{}\' does already exist."'.format(project_name))
    exit(1)
  if project_name in p.subcommands:
    print('echo "\'{}\' is a reserved keyword and thus cannot be used as project name."'.format(project_name))
    exit(1)
  with open(os.path.join(p.config_path, project_name + '_activate.sh'), 'w') as f:
    f.write('# Here you can enter the commands to run when activating the project.\n\n')
    f.write('echo "Setting up project \'{}\'..."\n'.format(project_name))
    if project_path:
      f.write('export {}_PATH="{}"\n'.format(project_name, project_path))
      f.write('cd ${}_PATH\n'.format(project_name))
  with open(os.path.join(p.config_path, project_name + '_deactivate.sh'), 'w') as f:
    f.write('# Here you can enter the commands to run when deactivating the project.\n\n')
    f.write('echo "Closing project \'{}\'..."\n'.format(project_name))


def delete_project(project_name: str) -> None:
  print('rm {}'.format(os.path.join(p.config_path, project_name + '_activate.sh')))
  print('rm {}'.format(os.path.join(p.config_path, project_name + '_deactivate.sh')))


def edit_project_activate(project_name: str) -> None:
  edit_file(project_name, 'activate')


def edit_project_deactivate(project_name: str) -> None:
  edit_file(project_name, 'deactivate')


def edit_file(project_name: str, file_type: str) -> None:
  editor = os.getenv('P_EDITOR', 'vim')
  file = os.path.join(p.config_path, '{}_{}.sh'.format(project_name, file_type))
  print('{} "{}"'.format(editor, file))


if __name__ == '__main__':
  os.makedirs(p.config_path, exist_ok=True)
  os.makedirs(p.cache_path, exist_ok=True)

  parser: argparse.ArgumentParser = p.get_parser(indirect=True)
  args = parser.parse_args()

  if args.command == 'activate' or args.command in p.find_projects():
    activate_project(args.project_name if hasattr(args, 'project_name') else args.command)
  elif args.command == 'deactivate':
    deactivate_project(args.project_name)
  elif args.command == 'edit-activate':
    edit_project_activate(args.project_name)
  elif args.command == 'edit-deactivate':
    edit_project_deactivate(args.project_name)
  elif args.command == 'new':
    new_project(args.project_name, args.project_path)
  elif args.command == 'delete':
    delete_project(args.project_name)
