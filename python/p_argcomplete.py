import argcomplete
import argparse
import sys

import p


def run_completer(parser: argparse.ArgumentParser, completer, command, point=None, **kwargs):
  cword_prequote, cword_prefix, _, comp_words, first_colon_pos = argcomplete.split_line(command)

  completions = completer._get_completions(
    comp_words, cword_prefix, cword_prequote, first_colon_pos)

  return completions


if __name__ == '__main__':
  parser: argparse.ArgumentParser = p.get_parser()
  completer = argcomplete.CompletionFinder(parser)

  user_input = p.program_name + ' '
  if 2 in sys.argv:
    user_input += sys.argv[2]
  completions = run_completer(parser, completer, p.program_name + ' ')

  for c in completions:
    print(c)
