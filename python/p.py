import argparse
import os
import sys
from typing import List


program_name: str = 'p'
config_path: str = os.getenv('P_CONFIG_DIR', os.path.join(os.path.expanduser('~'), '.config', program_name))
cache_path: str = os.path.join(os.path.expanduser('~'), '.cache', program_name)
return_source_file: str = os.path.join(cache_path, 'return_source.sh')
subcommands: List[str] = ['activate', 'deactivate', 'edit-activate', 'edit-deactivate', 'new',
                          'delete']


class IndirectArgumentParser(argparse.ArgumentParser):

  def _print_message(self, message, file=None):
    if message:
      file = sys.stdout
      file.write('echo "{}"\n'.format(message))


def find_projects() -> List[str]:
  try:
    return [i.replace('.sh', '') for i in os.listdir(config_path)]
  except FileNotFoundError:
    os.makedirs(config_path)
    return []


def get_parser(indirect=False) -> argparse.ArgumentParser:
  projects = find_projects()

  if indirect:
    parser = IndirectArgumentParser(program_name, description='Project environment management.')
  else:
    parser = argparse.ArgumentParser(program_name, description='Project environment management.')

  subparsers = parser.add_subparsers(dest='command')

  for project in projects:
    subparsers.add_parser(project)

  internal_parsers = {}
  for subcommand in subcommands:
    internal_parsers[subcommand] = subparsers.add_parser(subcommand)
    if subcommand == 'new':
      internal_parsers[subcommand].add_argument('project_name', type=str)
      internal_parsers[subcommand].add_argument('-p', '--project-path', type=str, default=False)
    else:
      internal_parsers[subcommand].add_argument('project_name', type=str, choices=projects)

  return parser
