# Default home where repos are cloned to etc.
export p_home="$HOME/code"

# Path to project files.
export p_project_files="$HOME/.config/p"

# Whether root is available or not.
export p_allow_sudo=false

# Enable or disbale currently active project before terminal prompt.
export p_fallback_prompt=true

# Default cmake build folder.
export p_cmake_build_dir="build"

# Default cmake generator. For make, use "Unix Makefiles"
export p_cmake_generator="Ninja"

# Amount of cores to utilize for the configured generator in cmake.
export p_cmake_cores=$(nproc)

# Default venv folder.
export p_venv_dir="venv"

# Git name.
export p_git_user_name="$(getent passwd "$(whoami)" | cut -d ':' -f 5 | cut -d ',' -f 1)"

# Git email.
export p_git_user_email="$(whoami)@$(hostname)"

# Enable/disable verbose outputs (mostly for debugging).
export p_verbose=false
