export p_fallback_prompt=false

prompt_virtualenv()
{
  local env='';

  if [[ -n $p_active_project ]]; then
    env=$p_active_project
  fi

  if [[ -n $env ]]; then
    prompt_segment green black
    print -Pn " $(basename $env) "
  fi
}
