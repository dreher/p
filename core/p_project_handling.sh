function _p_cdpr
{
  local root_path=$(eval "echo \$${p_active_project}_root")

  if [[ -d $root_path ]]; then
    pushd $root_path > /dev/null
  fi
}


function _p_source_project
{
  local project="$1"
  local project_sh="$p_project_files/$project.sh"

  if [[ -f "$project_sh" ]]; then
    source "$project_sh"
  else
    echo "Unknown project '$project'."
    return 1
  fi
}


function _p_activate_user_defined_project
{
  local project="$1"

  # Set p_active_project if not set (user selected top-level project).
  if _p_set_env p_active_project "$project"; then
    # If current shell does not offer higher level prompt support, use fallback.
    if $p_fallback_prompt; then
      export p_original_ps1="$PS1"
      export PS1="($p_active_project) $PS1"
    fi
  fi
}


function _p_deactivate_user_defined_project
{
  local project="$1"

  # Reset to default prompt in fallback mode
  if $p_fallback_prompt; then
    export PS1="$p_original_ps1"
    unset p_original_ps1
  fi

  if [[ "$project" == "$p_active_project" ]]; then
    unset p_active_project
  fi
}


function _p_run_on_project
{
  local project="$1"
  local command="$2"
  export p_current_project="$project"
  _p_call_if_defined "${project}_${command}"
  code=$?
  unset p_current_project
  return $code
}


function _p_run_recursive_on_deps
{
  local project="$1"
  shift
  local command="$1"
  shift
  for dep in $(_p_call_if_defined "${project}_deps"); do
    if _p_project_exists $dep; then
      if ! eval _p_$command $dep $@; then
        return 1
      fi
    else
      echo "Unknown dependency '$dep' in project '$project'."
      return 1
    fi
  done
}


function _p_run_reverted_recursive_on_deps
{
  local project="$1"
  shift
  local command="$1"
  shift
  for dep in $(_p_call_if_defined "${project}_deps" | tac); do
    if _p_project_exists $dep; then
      if ! eval _p_$command $dep $@; then
        return 1
      fi
    else
      echo "Unknown dependency '$dep' in project '$project'."
      return 1
    fi
  done
}


function _p_project_exists
{
  test -f "$p_project_files/$1.sh"
}
