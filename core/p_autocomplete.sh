# Define the autocompletion function, which forwards the coll to a Python script.
function _p_complete
{
  COMPREPLY="" #( $($P_PYTHON_EXEC "$p_root_dir/python/p_argcomplete.py" "${COMP_WORDS[1]}") )
}


# Register the autocompletion function with the p function.
complete -F _p_complete p
