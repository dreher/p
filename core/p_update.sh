function _p_update_help
{
  echo "Usage: p update <project> [--recursive|--pull]"
}


function _p_update
{
  local project="$1"
  if [[ -z "$project" ]] || [[ "$project" =~ ^-.*  ]]; then
    project="$p_active_project"
  else
    shift
  fi
  if [[ -z "$project" ]]; then
    echo "No project active to update.  You can also supply one as argument."
    return 1
  fi
  local recursive=false
  local pull=false

  while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        --recursive)
        recursive=true
        shift
        ;;
        -r)
        recursive=true
        shift
        ;;
        --pull)
        pull=true
        shift
        ;;
        -p)
        pull=true
        shift
        ;;
        *)
        echo "Unknown argument '$key'."
        shift
        ;;
    esac
  done

  if _p_set_env "p_dont_update_$project" true; then
    if ! _p_source_project $project; then
      _p_error "Couldn't source '$project'."
      return 1
    fi

    if ! _p_is_env_set "p_dont_activate_$project"; then
      if ! _p_activate $project; then
        _p_error "Couldn't activate '$project'."
        return 1
      fi
    fi

    if $recursive; then
      flags=$($recursive && echo "-r")" "$($pull && echo "-p")
      if ! _p_run_recursive_on_deps $project update $flags; then
        _p_error "Couldn't update dependencies of '$project'."
        return 1
      fi
    fi

    _p_hero_sep "Updating '$project'."
    if ! _p_run_on_project $project update; then
      _p_error "Couldn't update project '$project'."
      return 1
    fi
  else
    _p_hero_sep "Updating '$project': already updated."
  fi
}
