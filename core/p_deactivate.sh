function _p_deactivate_help
{
  echo "Usage: p deactivate"
}


function _p_deactivate
{
  local project="$1"
  if [[ -z "$project" ]]; then
    project="$p_active_project"
  fi

  # Ensure that project is activated.
  if _p_unset_env "p_dont_activate_${project}"; then
    if ! _p_source_project $project; then
      _p_error "Couldn't source '$project'."
      return 1
    fi
    _p_echo "Deactivating '$project'."
    if ! _p_run_on_project $project deactivate; then
      _p_error "Couldn't deactivate '$project'."
      return 1
    fi
    if ! _p_run_reverted_recursive_on_deps $project deactivate; then
      _p_error "Couldn't deactivate dependencies of '$project'."
      return 1
    fi

    _p_deactivate_user_defined_project $project
  else
    if [[ -z "$project" ]]; then
      echo "No project active."
    else
      echo "Project '$project' not active."
    fi
    return 1
  fi
}
