function p_env_backup_and_set
{
  variable_name=$1
  old_value=$(eval "echo \$$variable_name")
  new_value=$2
  backup_variable_name="p_backup_${p_current_project}_$variable_name"
  backup_variable_value=$(eval "echo \$$backup_variable_name")

  if [[ -z "$backup_variable_value" ]]; then
    eval "export $backup_variable_name=\"\$$variable_name\""
  fi

  eval "export $variable_name=\"$new_value\""
  _p_echo "Set '$variable_name' from '$old_value' to '$new_value'."
}


function p_env_restore
{
  variable_name=$1

  eval "export $variable_name=\"\$p_backup_${p_current_project}_$variable_name\""
  unset "p_backup_${p_current_project}_$variable_name"
  _p_echo "Restored '$variable_name' to '$(eval "echo \$$variable_name")'."
}


function p_os_is_ubuntu
{
  if lsb_release -d | grep -q "Ubuntu" && lsb_release -sc | grep -q "$1"; then
    return 0
  else
    return 1
  fi
}


function p_host_is
{
  if hostname | grep -q "$1"; then
    return 0
  else
    return 1
  fi
}


function p_current_project_is_active
{
  if [[ "$p_current_project" == "$p_active_project" ]]; then
    return 0
  else
    return 1
  fi
}


function p_system_packages
{
  if $p_allow_sudo; then
    echo "Installing system packages: "
    for package_name in "$@"
    do
      printf "  '$package_name': "
      if ! dpkg -s "$package_name" | grep Status ; then
        sudo apt install "$package_name" -y
      fi
    done
    printf "\n"
  else
    echo "Checking system packages: "
    for package_name in "$@"
    do
      printf "  '$package_name': "
      if ! dpkg -s "$package_name" | grep Status ; then
        echo "not installed"
        echo "Package '$package_name' missing and not privileged to install"
        return 1
      fi
    done
    printf "\n"
  fi
}


function p_update_from_git
{
  local proj_path=$(eval "echo \$${p_current_project}_root")
  local proj_repo=$(eval "echo \$${p_current_project}_repo")
  local proj_repo_co=$(eval "echo \$${p_current_project}_repo_co")
  local proj_pull=$(eval "echo \$${p_current_project}_pull")

  if [[ ! -d "$proj_path" ]]; then
    echo "Cloning repository"
    git clone "$proj_repo" "$proj_path"
    if [[ -n "$proj_repo_co" ]]; then
      pushd "$proj_path" > /dev/null
      echo "Switching branch to $proj_repo_co"
      git checkout "$proj_repo_co"
      popd > /dev/null
    fi
  else
    # Print info.
    pushd "$proj_path" > /dev/null
    echo "Git status"
    git status -bs
    echo

    # Check if current ref is same as *_repo_co.
    echo "Git target ref"
    local current_sym_ref=$(git rev-parse --abbrev-ref HEAD)
    # If current_sym_ref is "HEAD", no particular branch is checked out, but maybe a tag.
    if [[ "$current_sym_ref" == "HEAD" ]]; then
      # Try to get tag of current commit.
      current_sym_ref=$(git --no-pager tag --points-at HEAD)
    fi
    if [[ -z "$proj_repo_co" ]]; then
      echo "  Project does not specify a target ref."
    elif [[ ! "$current_sym_ref" == "$proj_repo_co" ]]; then
      echo "  Project is not on target ref '$proj_repo_co'."
    else
      echo "  Project target ref is valid."
    fi
    echo

    echo "Git log"
    git --no-pager log --pretty=format:"%h (%Cred%an%Creset), %ar: %Cgreen%s%Creset" -n5
    echo
    echo

    git remote set-url origin "$proj_repo"

    if $proj_pull; then
      echo "Fetching latest code"
      git pull || true
    else
      echo "\$${project}_pull is not 'true', skipping 'git pull'"
    fi

    popd > /dev/null
  fi
}


function p_cmake_build
{
  local proj_path=""
  if [[ -n "$1" ]] && [[ ! "$1" =~ ^-.*  ]]; then
    proj_path="$1"
    shift
  else
    proj_path=$(eval "echo \$${p_current_project}_root")
  fi

  pushd "$proj_path" > /dev/null

  mkdir $p_cmake_build_dir 2> /dev/null
  cd $p_cmake_build_dir

  cmake -G "$p_cmake_generator" -DCMAKE_BUILD_TYPE=RelWithDebInfo -DFORCE_COLORED_OUTPUT=1 -DCMAKE_CXX_COMPILER_LAUNCHER=ccache -DCMAKE_CC_COMPILER_LAUNCHER=ccache -DCMAKE_CUDA_COMPILER_LAUNCHER=ccache $@ .. || return 1
  cmake --build . -- -j "$p_cmake_cores" || return 1

  popd > /dev/null
}


function p_cmake_build_install
{
  local proj_path=""
  if [[ -n "$1" ]] && [[ ! "$1" =~ ^-.*  ]]; then
    proj_path="$1"
    shift
  else
    proj_path=$(eval "echo \$${p_current_project}_root")
  fi

  pushd "$proj_path" > /dev/null

  mkdir $p_cmake_build_dir 2> /dev/null
  cd $p_cmake_build_dir

  cmake -G "$p_cmake_generator" -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX:PATH="$proj_path/$p_cmake_build_dir" -DFORCE_COLORED_OUTPUT=1 -DCMAKE_CXX_COMPILER_LAUNCHER=ccache -DCMAKE_CC_COMPILER_LAUNCHER=ccache -DCMAKE_CUDA_COMPILER_LAUNCHER=ccache $@ .. || return 1
  cmake --build . --target install -- -j "$p_cmake_cores" || return 1

  popd > /dev/null
}


function p_venv_activate
{
  local proj_path=""
  if [[ -n "$1" ]]; then
    proj_path="$1"
  else
    proj_path=$(eval "echo \$${p_current_project}_root")
  fi

  if [[ ! -d "$proj_path" ]]; then
    return
  fi

  pushd "$proj_path" > /dev/null

  if [[ -d "$p_venv_dir" ]]; then
    source "$p_venv_dir/bin/activate"
  fi

  popd > /dev/null
}


function p_venv_setup
{
  local proj_path=""
  if [[ -n "$1" ]]; then
    proj_path="$1"
  else
    proj_path=$(eval "echo \$${p_current_project}_root")
  fi

  pushd "$proj_path" > /dev/null

  if [[ ! -d "$p_venv_dir" ]]; then
    virtualenv -p /usr/bin/python3 venv
  fi

  source "$p_venv_dir/bin/activate"
  if [[ -f requirements.txt ]]; then
    pip install -r requirements.txt
  fi

  popd > /dev/null
}
