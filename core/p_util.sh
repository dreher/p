function _p_is_env_set
{
  local varname=$1

  local current_varval="$(eval "echo \$$varname")"
  if [[ -n "$current_varval" ]]; then
    return 0
  else
    return 1
  fi
}


function _p_set_env
{
  local varname=$1
  local varval=$2

  local current_varval="$(eval "echo \$$varname")"
  if [[ -z "$current_varval" ]]; then
    eval "export $varname=$varval"
  else
    return 1
  fi
}


function _p_unset_env
{
  local varname=$1

  local current_varval="$(eval "echo \$$varname")"
  if [[ -n "$current_varval" ]]; then
    eval "unset $varname"
  else
    return 1
  fi
}


function _p_call_if_defined
{
  local fn="$1"
  #if [[ -n "$(LC_ALL=C type -t $fn)" ]] && [[ "$(LC_ALL=C type -t $fn)" = function ]]; then
  if typeset -f $fn > /dev/null; then
    $fn
  fi
}


function _p_hero_sep
{
  if which toilet > /dev/null 2>&1; then
    echo
    toilet -t -f smmono12 "$1"
    echo
  else
    echo ""
    echo "################################################################################"
    echo "################################################################################"
    echo "## $1"
    echo "################################################################################"
    echo "################################################################################"
    echo ""
  fi
}


function _p_echo
{
  if $p_verbose; then
    echo $@
  fi
}


function _p_error
{
  echo "p error: $@"
}
