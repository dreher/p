# Figure out the p root path if not set.
if [[ -z "$p_root_dir" ]]; then
  if [[ -n "$BASH" ]]; then export p_root_dir=$BASH_SOURCE
  elif [[ -n "$TMOUT" ]]; then export p_root_dir=${.sh.file}
  elif [[ -n "$ZSH_NAME" ]]; then export p_root_dir=${(%):-%x}
  elif [[ ${0##*/} = dash ]]; then x=$(lsof -p $$ -Fn0 | tail -1); export p_root_dir=${x#n}
  else export p_root_dir=$0
  fi
  export p_root_dir="$(cd "$(dirname $p_root_dir)/.." && pwd)"
fi


# Disable virtualenv prompt, use own.
export VIRTUAL_ENV_DISABLE_PROMPT=true


# Set defaults if not set from user.
source "$p_root_dir/core/set_config_defaults.sh"

# Core utility functions.
source "$p_root_dir/core/p_util.sh"

# Project management related functions.
source "$p_root_dir/core/p_project_handling.sh"

# p activate functionality.
source "$p_root_dir/core/p_activate.sh"

# p deactivate functionality.
source "$p_root_dir/core/p_deactivate.sh"

# p update functionality.
source "$p_root_dir/core/p_update.sh"

# p build functionality.
source "$p_root_dir/core/p_build.sh"


function _p_help
{
  echo "Usage: p [activate|deactivate|update|build] project"
}


# Main function.
function p
{
  local command="activate"

  # Check for command.
  if [[ "$1" == "activate" || "$1" == "deactivate" || "$1" == "update" || "$1" == "build" ]]; then
    command="$1"
    shift
  elif [[ "$1" == "--help" || "$1" == "-h" ]]; then
    _p_help
    return 0
  else
    command="activate"
  fi

  case $command in
    activate)
    _p_activate $@
    ;;
    deactivate)
    _p_deactivate $@
    ;;
    update)
    (_p_update $@)
    ;;
    build)
    (_p_build $@)
    ;;
    *)
    echo "Unknown command '$command'."
    return 1
  esac
}


# Autocompletion for p.
source "$p_root_dir/core/p_autocomplete.sh"

# Client tools exposed to the user.
source "$p_root_dir/core/client_tools.sh"


export p_installed=true
