function _p_activate_help
{
  echo "Usage: p activate <project>"
}


function _p_activate
{
  local project="$1"

  _p_activate_user_defined_project $project

  # Ensure that project is not activated.
  if _p_set_env "p_dont_activate_${project}" true; then
    if ! _p_source_project $project activate; then
      _p_error "Couldn't source '$project'."
      _p_deactivate_user_defined_project $project
      return 1
    fi
    if ! _p_run_recursive_on_deps $project activate; then
      _p_error "Couldn't activate dependencies of '$project'."
      _p_deactivate_user_defined_project $project
      return 1
    fi
    _p_echo "Activating '$project'."
    if ! _p_run_on_project $project activate; then
      _p_error "Couldn't activate '$project'."
      _p_deactivate_user_defined_project $project
      return 1
    fi
  else
    _p_echo "Project '$project' already active."
  fi

  _p_cdpr
  return 0
}
