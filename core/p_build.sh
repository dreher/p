function _p_build_help
{
  echo "Usage: p build <project> [--recursive]"
}


function _p_build
{
  local project="$1"
  if [[ -z "$project" ]] || [[ "$project" =~ ^-.*  ]]; then
    project="$p_active_project"
  else
    shift
  fi
  if [[ -z "$project" ]]; then
    echo "No project active to build.  You can also supply one as argument."
    return 1
  fi
  local recursive=false

  while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        --recursive)
        recursive=true
        shift
        ;;
        -r)
        recursive=true
        shift
        ;;
        *)
        echo "Unknown argument '$key'."
        shift
        ;;
    esac
  done

  # Prerequisites (project active, project deps built if recursive).
  if _p_set_env "p_dont_build_$project" true; then
    if ! _p_source_project $project; then
      _p_error "Couldn't source '$project'."
      return 1
    fi

    if ! _p_is_env_set "p_dont_activate_$project"; then
      if ! _p_activate $project; then
        _p_error "Couldn't activate '$project'."
        return 1
      fi
    fi

    if $recursive; then
      flags=$($recursive && echo "-r")
      if ! _p_run_recursive_on_deps $project build $flags; then
        _p_error "Couldn't build dependencies of '$project'."
        return 1
      fi
    fi

    _p_hero_sep "Building '$project'."
    if ! _p_run_on_project $project build; then
      _p_error "Couldn't build '$project'."
      return 1
    fi
  else
    _p_hero_sep "Building '$project': already built."
  fi
}
