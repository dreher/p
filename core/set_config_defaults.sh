# Default home where repos are cloned to etc.
if [[ -z "$p_home" ]]; then
  export p_home="$HOME/code"
fi
if [[ ! -d "$p_home" ]]; then
  mkdir -p "$p_home"
fi

# Path to project files.
if [[ -z "$p_project_files" ]]; then
  export p_project_files="$HOME/.config/p"
fi
if [[ ! -d "$p_project_files" ]]; then
  mkdir -p "$p_project_files"
fi

# Whether root is available or not.
if [[ -z "$p_allow_sudo" ]]; then
  export p_allow_sudo=false
fi

if [[ -z "$p_fallback_prompt" ]]; then
  export p_fallback_prompt=true
fi

# Default cmake build folder.
if [[ -z "$p_cmake_build_dir" ]]; then
  export p_cmake_build_dir="build"
fi

# Default cmake generator.
if [[ -z "$p_cmake_generator" ]]; then
  export p_cmake_generator="Ninja"
fi

if [[ -z "$p_cmake_cores" ]]; then
  export p_cmake_cores=$(nproc)
fi

# Default venv folder.
if [[ -z "$p_venv_dir" ]]; then
  export p_venv_dir="venv"
fi

# Git name
if [[ -z "$p_git_user_name" ]]; then
  export p_git_user_name="$(getent passwd "$(whoami)" | cut -d ':' -f 5 | cut -d ',' -f 1)"
fi

# Git email
if [[ -z "$p_git_user_email" ]]; then
  export p_git_user_email="$(whoami)@$(hostname)"
fi

# Enable/disable verbose outputs (mostly for debugging).
if [[ -z "$p_verbose" ]]; then
  export p_verbose=false
fi
